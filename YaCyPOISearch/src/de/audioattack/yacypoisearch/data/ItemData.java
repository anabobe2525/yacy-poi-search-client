package de.audioattack.yacypoisearch.data;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemData implements Parcelable {

    private String title = null;

    private String summary = null;

    private GeoPoint geoPoint = null;

    /**
     * 
     * @param title
     * @param summary
     * @param geoPoint
     */
    public ItemData(
            final String title,
            final String summary,
            final GeoPoint geoPoint) {
        this.title = title;
        this.summary = summary;
        this.geoPoint = geoPoint;
    }

    /**
     * 
     * @param in
     */
    private ItemData(final Parcel in) {
        title = in.readString();
        summary = in.readString();
        geoPoint = new GeoPoint(in.readInt(), in.readInt(), in.readInt());
    }

    public static final Parcelable.Creator<ItemData> CREATOR
    = new Parcelable.Creator<ItemData>() {
        public ItemData createFromParcel(final Parcel in) {
            return new ItemData(in);
        }

        public ItemData[] newArray(final int size) {
            return new ItemData[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel out, final int flags) {
        out.writeString(title);
        out.writeString(summary);
        out.writeInt(geoPoint.getLatitudeE6());
        out.writeInt(geoPoint.getLongitudeE6());
        out.writeInt(geoPoint.getAltitude());
    }

    public final String getTitle() {
        return title;
    }

    public final void setTitle(String title) {
        this.title = title;
    }

    public final String getSummary() {
        return summary;
    }

    public final void setSummary(String summary) {
        this.summary = summary;
    }

    public final GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public final void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public final OverlayItem toOverlayItem() {
        return new OverlayItem(title, summary, geoPoint);
    }

}
