package de.audioattack.yacypoisearch.activities;

import de.audioattack.yacypoisearch.R;
import de.audioattack.yacypoisearch.R.layout;
import android.app.Activity;  
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
    }
 
    @Override
    protected void onResume() {
		super.onResume();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				SplashActivity.this.finish();
				Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
				SplashActivity.this.startActivity(mainIntent);
			}
		}, 1000);
	}
}