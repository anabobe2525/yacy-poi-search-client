package de.audioattack.yacypoisearch.views;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.ResourceProxy;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;

public class DrawingOverlay extends Overlay {

    final List<Circle> circles = new ArrayList<Circle>();

    public DrawingOverlay(final ResourceProxy pResourceProxy) {
        super(pResourceProxy);
    }

    public DrawingOverlay(final Context context) {
        super(context);
    }

    @Override
    protected final void draw(
            final Canvas canvas,
            final MapView mapView,
            final boolean shadow) {

        if (shadow) {
            return;
        }

        final BoundingBoxE6 bbox = mapView.getBoundingBox();

        for (final Circle circle : circles) {
            if (circle.isVisible(bbox, mapView)) {
                circle.draw(canvas, mapView, shadow);
            }
        }

    }

    public boolean add(final Circle circle) {
        return this.circles.add(circle);
    }

    public boolean remove(final Circle circle) {
        return this.circles.remove(circle);
    }

    public static final int metersToRadius(
            final float meters,
            final MapView mapView,
            final int latitudeE6) {
          return (int) (
                  mapView.getProjection().metersToEquatorPixels(meters)
                  / Math.cos(Math.toRadians(latitudeE6 / 1e6)));
        }

    public static class Circle implements Parcelable {
        private final GeoPoint center;
        private final float radius;
        private final String text;
        private final int bgColor;

        private final Paint bgPaint;

        /**
         * 
         * @param center
         * @param radius radius in m
         * @param text
         * @param bgColor
         */
        public Circle(
                final GeoPoint center,
                final float radius,
                final String text,
                final int bgColor) {
            this.center = center;
            this.radius = radius;
            this.text = text;
            this.bgColor = bgColor;

            bgPaint = new Paint();
            bgPaint.setColor(bgColor);
            bgPaint.setAntiAlias(true);
//            bgPaint.ascent();
        }

        public boolean isVisible(
                final BoundingBoxE6 bbox,
                final MapView mapView) {

            // TODO Auto-generated method stub
            return true;
        }

        public void draw(
                final Canvas canvas,
                final MapView mapView,
                final boolean shadow) {
            Point out = new Point();
            mapView.getProjection().toPixels(center, out);
            final float r = DrawingOverlay.metersToRadius(radius, mapView, center.getLatitudeE6());
            canvas.drawCircle(out.x, out.y, r, bgPaint);
        }

        @Override
        public int describeContents() {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void writeToParcel(final Parcel dest,final  int flags) {
            // TODO Auto-generated method stub
            
        }
    }

}
