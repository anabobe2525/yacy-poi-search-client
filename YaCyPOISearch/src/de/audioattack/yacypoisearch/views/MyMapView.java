package de.audioattack.yacypoisearch.views;

import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;


public class MyMapView extends MapView {

    /** Used for logging. */
    private static final String TAG = MapView.class.getSimpleName();;

    private boolean automaticLocationUpdates = true;

    protected MyMapView(
            final Context context,
            final int arg1,
            final ResourceProxy arg2,
            final MapTileProviderBase arg3,
            final Handler arg4,
            final AttributeSet arg5) {
        super(context, arg1, arg2, arg3, arg4, arg5);
    }

    public MyMapView(
            final Context context,
            final int tileSizePixels,
            final ResourceProxy resourceProxy,
            final MapTileProviderBase aTileProvider,
            final Handler tileRequestCompleteHandler) {
        super(context, tileSizePixels, resourceProxy, aTileProvider,
                tileRequestCompleteHandler);
    }

    public MyMapView(
            final Context context,
            final int tileSizePixels,
            final ResourceProxy resourceProxy,
            final MapTileProviderBase aTileProvider) {
        super(context, tileSizePixels, resourceProxy, aTileProvider);
    }

    public MyMapView(
            final Context context,
            final int tileSizePixels,
            final ResourceProxy resourceProxy) {
        super(context, tileSizePixels, resourceProxy);
    }

    public MyMapView(final Context context, final int tileSizePixels) {
        super(context, tileSizePixels);
    }

    public MyMapView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public final boolean onTouchEvent(final MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            automaticLocationUpdates = false;
            final BoundingBoxE6 bb = this.getBoundingBox();
            final GeoPoint p = bb.getCenter();
            Log.d(TAG, "map moved by user to lat = " + p.getLatitudeE6() + ", lon = " + p.getLongitudeE6() + ", zoom = " + this.getZoomLevel());
        }
        return super.onTouchEvent(event);
    }

    public final boolean isAutomaticLocationUpdatesEnabled() {
        return automaticLocationUpdates;
    }

    public final void setAutomaticLocationUpdatesEnabled(
            final boolean enabled) {
        this.automaticLocationUpdates = enabled;
    }

}
